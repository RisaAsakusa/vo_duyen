<?php $id="topics";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<div class="p-topics">

	<div class="c-title1">
		<h2><img src="assets/image/common/title_02.png" width="184" height="45" alt=""></h2>
	</div>

	<!-- section1 -->
	<section class="p-topics1 l-container">
		<div class="c-text1">
			<p>『ココダヨ』では、毎月1日 午前10時 に「訓練地震」を実施いたします。 <br>訓練地震では、本当の地震が発生した場合と同様に、ココダヨユーザ全端末に最大震度予測5弱の地震が発生したという「訓練の」通知を発信いたします。次回実施予定は 2017年6月1日 木曜日 午前10時 です。</p>
		</div>

		<ul class="accordion">
			<li class="open">
				<div class="accordion--headline">
					<h3><span>2017.3.14</span>『ココダヨ』アプリiOS版を、1.1.9にバージョンアップしました。</h3>
				</div>
				<div class="accordion--content">
					<p>たいへんお待たせ致しました。<br>リーダーになって自分のグループを作ることができるようになった<br>『ココダヨ』アプリAndroid版バージョン 1.1.0 をGoogle Play にて公開しました。<br>ご家族全員で Android をお使いの皆様、この機会に是非おためし下さい。</p>
					<a href="">Press Release →</a>
					<p>バージョン 1.1.0 の修正点<br>グループ作成機能：グループを作成してリーダーとなり、メンバーを招待できるようになりました。<br>データ引継ぎ機能：アカウント(メールアドレスとパスワード)を登録して、データの引継ぎが行えるようになりました。</p>
				</div>
			</li>
			
			<li>
				<div class="accordion--headline">
					<h3><span>2017.3.14</span>『ココダヨ』アプリiOS版を、1.1.9にバージョンアップしました。</h3>
				</div>
				<div class="accordion--content">
					<p>たいへんお待たせ致しました。<br>リーダーになって自分のグループを作ることができるようになった<br>『ココダヨ』アプリAndroid版バージョン 1.1.0 をGoogle Play にて公開しました。<br>ご家族全員で Android をお使いの皆様、この機会に是非おためし下さい。</p>
					<a href="">Press Release →</a>
					<p>バージョン 1.1.0 の修正点<br>グループ作成機能：グループを作成してリーダーとなり、メンバーを招待できるようになりました。<br>データ引継ぎ機能：アカウント(メールアドレスとパスワード)を登録して、データの引継ぎが行えるようになりました。</p>
				</div>
			</li>

			<li>
				<div class="accordion--headline">
					<h3><span>2017.3.14</span>『ココダヨ』アプリiOS版を、1.1.9にバージョンアップしました。</h3>
				</div>
				<div class="accordion--content">
					<p>たいへんお待たせ致しました。<br>リーダーになって自分のグループを作ることができるようになった<br>『ココダヨ』アプリAndroid版バージョン 1.1.0 をGoogle Play にて公開しました。<br>ご家族全員で Android をお使いの皆様、この機会に是非おためし下さい。</p>
					<a href="">Press Release →</a>
					<p>バージョン 1.1.0 の修正点<br>グループ作成機能：グループを作成してリーダーとなり、メンバーを招待できるようになりました。<br>データ引継ぎ機能：アカウント(メールアドレスとパスワード)を登録して、データの引継ぎが行えるようになりました。</p>
				</div>
			</li>

			<li>
				<div class="accordion--headline">
					<h3><span>2017.3.14</span>『ココダヨ』アプリiOS版を、1.1.9にバージョンアップしました。</h3>
				</div>
				<div class="accordion--content">
					<p>たいへんお待たせ致しました。<br>リーダーになって自分のグループを作ることができるようになった<br>『ココダヨ』アプリAndroid版バージョン 1.1.0 をGoogle Play にて公開しました。<br>ご家族全員で Android をお使いの皆様、この機会に是非おためし下さい。</p>
					<a href="">Press Release →</a>
					<p>バージョン 1.1.0 の修正点<br>グループ作成機能：グループを作成してリーダーとなり、メンバーを招待できるようになりました。<br>データ引継ぎ機能：アカウント(メールアドレスとパスワード)を登録して、データの引継ぎが行えるようになりました。</p>
				</div>
			</li>

			<li>
				<div class="accordion--headline">
					<h3><span>2017.3.14</span>『ココダヨ』アプリiOS版を、1.1.9にバージョンアップしました。</h3>
				</div>
				<div class="accordion--content">
					<p>たいへんお待たせ致しました。<br>リーダーになって自分のグループを作ることができるようになった<br>『ココダヨ』アプリAndroid版バージョン 1.1.0 をGoogle Play にて公開しました。<br>ご家族全員で Android をお使いの皆様、この機会に是非おためし下さい。</p>
					<a href="">Press Release →</a>
					<p>バージョン 1.1.0 の修正点<br>グループ作成機能：グループを作成してリーダーとなり、メンバーを招待できるようになりました。<br>データ引継ぎ機能：アカウント(メールアドレスとパスワード)を登録して、データの引継ぎが行えるようになりました。</p>
				</div>
			</li>

			<li>
				<div class="accordion--headline">
					<h3><span>2017.3.14</span>『ココダヨ』アプリiOS版を、1.1.9にバージョンアップしました。</h3>
				</div>
				<div class="accordion--content">
					<p>たいへんお待たせ致しました。<br>リーダーになって自分のグループを作ることができるようになった<br>『ココダヨ』アプリAndroid版バージョン 1.1.0 をGoogle Play にて公開しました。<br>ご家族全員で Android をお使いの皆様、この機会に是非おためし下さい。</p>
					<a href="">Press Release →</a>
					<p>バージョン 1.1.0 の修正点<br>グループ作成機能：グループを作成してリーダーとなり、メンバーを招待できるようになりました。<br>データ引継ぎ機能：アカウント(メールアドレスとパスワード)を登録して、データの引継ぎが行えるようになりました。</p>
				</div>
			</li>
			
			<li>
				<div class="accordion--headline">
					<h3><span>2017.3.14</span>『ココダヨ』アプリiOS版を、1.1.9にバージョンアップしました。</h3>
				</div>
				<div class="accordion--content">
					<p>たいへんお待たせ致しました。<br>リーダーになって自分のグループを作ることができるようになった<br>『ココダヨ』アプリAndroid版バージョン 1.1.0 をGoogle Play にて公開しました。<br>ご家族全員で Android をお使いの皆様、この機会に是非おためし下さい。</p>
					<a href="">Press Release →</a>
					<p>バージョン 1.1.0 の修正点<br>グループ作成機能：グループを作成してリーダーとなり、メンバーを招待できるようになりました。<br>データ引継ぎ機能：アカウント(メールアドレスとパスワード)を登録して、データの引継ぎが行えるようになりました。</p>
				</div>
			</li>

			<li>
				<div class="accordion--headline">
					<h3><span>2017.3.14</span>『ココダヨ』アプリiOS版を、1.1.9にバージョンアップしました。</h3>
				</div>
				<div class="accordion--content">
					<p>たいへんお待たせ致しました。<br>リーダーになって自分のグループを作ることができるようになった<br>『ココダヨ』アプリAndroid版バージョン 1.1.0 をGoogle Play にて公開しました。<br>ご家族全員で Android をお使いの皆様、この機会に是非おためし下さい。</p>
					<a href="">Press Release →</a>
					<p>バージョン 1.1.0 の修正点<br>グループ作成機能：グループを作成してリーダーとなり、メンバーを招待できるようになりました。<br>データ引継ぎ機能：アカウント(メールアドレスとパスワード)を登録して、データの引継ぎが行えるようになりました。</p>
				</div>
			</li>

			<li>
				<div class="accordion--headline">
					<h3><span>2017.3.14</span>『ココダヨ』アプリiOS版を、1.1.9にバージョンアップしました。</h3>
				</div>
				<div class="accordion--content">
					<p>たいへんお待たせ致しました。<br>リーダーになって自分のグループを作ることができるようになった<br>『ココダヨ』アプリAndroid版バージョン 1.1.0 をGoogle Play にて公開しました。<br>ご家族全員で Android をお使いの皆様、この機会に是非おためし下さい。</p>
					<a href="">Press Release →</a>
					<p>バージョン 1.1.0 の修正点<br>グループ作成機能：グループを作成してリーダーとなり、メンバーを招待できるようになりました。<br>データ引継ぎ機能：アカウント(メールアドレスとパスワード)を登録して、データの引継ぎが行えるようになりました。</p>
				</div>
			</li>

			<li class="last">
				<div class="accordion--headline">
					<h3><span>2017.3.14</span>『ココダヨ』アプリiOS版を、1.1.9にバージョンアップしました。</h3>
				</div>
				<div class="accordion--content">
					<p>たいへんお待たせ致しました。<br>リーダーになって自分のグループを作ることができるようになった<br>『ココダヨ』アプリAndroid版バージョン 1.1.0 をGoogle Play にて公開しました。<br>ご家族全員で Android をお使いの皆様、この機会に是非おためし下さい。</p>
					<a href="">Press Release →</a>
					<p>バージョン 1.1.0 の修正点<br>グループ作成機能：グループを作成してリーダーとなり、メンバーを招待できるようになりました。<br>データ引継ぎ機能：アカウント(メールアドレスとパスワード)を登録して、データの引継ぎが行えるようになりました。</p>
				</div>
			</li>
		</ul>	

		<div class="c-pager">
			<a class="c-pager__prev" href="">前へ</a>
			<a class="c-pager__next" href="">次へ</a>
		</div>		
	</section>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>