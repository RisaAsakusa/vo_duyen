<!DOCTYPE html>
<html lang="ja" id="pagetop">
<head>
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/meta.php'); ?>
<link href="/assets/css/style.css" rel="stylesheet">
<link href="/assets/js/slick/slick.css" rel="stylesheet" type="text/css" />
<script src="/assets/js/jquery-1.12.4.min.js"></script>
<script src="/assets/js/functions.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/slick/slick.js"></script>
</head>
<body class="page-<?php echo $id; ?>">

<?php
//==============================================
// header PC
//============================================== ?>
<header class="c-header">
	<!-- GLOBAL MENU -->
	<div class="c-globalMenu pc-only">
		<div class="c-globalMenu__inner">
			<div class="c-globalMenu__logo">
				<a href=""><img src="/assets/image/common/logo_01.png" width="182" height="52" alt=""></a>
				
			</div>
			<nav class="c-globalMenu__gnav">
				<ul>
					<li><a href="/index.php">TOP</a></li>
					<li><a href="/qa.php">Q&amp;A</a></li>
					<li><a href="/topics.php">TOPICS</a></li>
				</ul>
			</nav>
			<div class="c-globalMenu__app">
				<p>今すぐ無料でダウンロード </p>
				<a href=""><img src="/assets/image/common/app_01.png" width="121" height="36" alt=""></a>
				<a href=""><img src="/assets/image/common/app_02.png" width="120" height="36" alt=""></a>				
			</div>
		</div>
	</div>

	<!-- GLOBAL MENU SP --> 
	<div class="c-globalMenuSP sp-only">
		<div class=""></div>
		<div class="menu-section">
			<div class="menu-toggle">
				<div class="one"></div>
				<div class="two"></div>
				<div class="three"></div>
			</div>
			<nav>
				<ul role="navigation" class="hidden sp-only">
					<li><a href="/index.php">TOP</a></li>					
					<li><a href="/topics.php">TOPICS</a></li>
					<li><a href="/qa.php">Q&amp;A</a></li>
					<li>
						<p>今すぐ無料でダウンロード </p>
						<a href=""><img src="/assets/image/common/app_01.png" width="121" height="36" alt=""></a>
						<a href=""><img src="/assets/image/common/app_02.png" width="120" height="36" alt=""></a>
					</li>
				</ul>
			</nav>
		</div>

		<div class="c-logo">
			<div class="img1"></div>
		</div>
		<div class="c-SNS">
			<div class="img2"></div>
			<div class="img3"></div>
		</div>
	</div>

	<!-- TOP -->
	<div class="c-top">
		<div class="c-top__banner">			
			<div class="c-logo">
				<img src="/assets/image/common/logo_02.png" width="232" height="243" alt="">
			</div>
		</div>
		<div class="c-top__threePoints">
			<div class="c-threePoints l-container">
				<div class="c-threePoints__imgPhone">
					<img src="/assets/image/common/img_02.png" width="244" height="471" alt="">
				</div>
				<div class="c-threePoints__text">
					<p>夏野剛 監修アプリ「ココダヨ」</p>
					<h2>つながりは、希望に変わる。</h2>
					<div class="c-list1">
						<div class="c-list1__card">
							<img src="/assets/image/common/point_01.png" width="" height="" alt="">
							<p>震度5以上の<br>地震を即通知</p>
						</div>
						<div class="c-list1__card">
							<img src="/assets/image/common/point_02.png" width="" height="" alt="">
							<p>家族間チャットで<br>会話可能</p>
						</div>
						<div class="c-list1__card">
							<img src="/assets/image/common/point_03.png" width="" height="" alt="">
							<p>家族の安否＆<br>居場所がわかる</p>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>
	
</header>

