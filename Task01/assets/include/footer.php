<?php
//==============================================
// footer
//============================================== ?>
<footer class="c-footer">
	<div class="c-footer__inner l-container">
		<h2>２週間無料体験できます</h2>
		<div class="c-footApp">
			<a href=""><img src="/assets/image/common/app_01.png" width="138" height="40" alt=""></a>
			<a class="ml" href=""><img src="/assets/image/common/app_02.png" width="138" height="40" alt=""></a>
		</div>
		<div class="c-footInfo">
			<img src="/assets/image/common/logo_03.png" width="173" height="50" alt="">
			<p>ご利用規約　会社情報　個人情報保護方針<br class="sp-only">　情報セキュリティ基本方針</p>
			<div class="c-footInfo--media pc-only">
				<a href=""><img src="/assets/image/common/icon_01.png" width="43" height="42" alt=""></a>
				<a href=""><img src="/assets/image/common/icon_02.png" width="43" height="42" alt=""></a>					
			</div>
		</div>
		<div class="c-footCopy">
			<p>※製品に関するお問合せは、App Store/Google Play、<br class="sp-only">またはアプリケーション内のメニューからお願いします<br>Copyright© 2016 GENETEC Corp.<br class="sp-only">All Rights Reserved.</p>
		</div>
	</div>
</footer>


<script src="/assets/js/functions.js"></script>
<script>
	$(window).bind('scroll', function () {
		if ($(window).scrollTop() > 50) {
			$('.c-globalMenu').addClass('fixed');
		} else {
			$('.c-globalMenu').removeClass('fixed');
		}
	});
	$(window).bind('scroll', function () {
		if ($(window).scrollTop() > 50) {
			$('.c-globalMenuSP').addClass('fixed');
		} else {
			$('.c-globalMenuSP').removeClass('fixed');
		}
	});

	$(".menu-toggle").on('click', function() {
		$(this).toggleClass("on");
		$('.menu-section').toggleClass("on");
		$("nav ul").toggleClass('hidden');
	});

</script>
<script type="text/javascript">
	/* YouTube Api */
	var tag = document.createElement('script');
	tag.src = "https://www.youtube.com/iframe_api";
	var firstScriptTag = document.getElementsByTagName('script')[0];
	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

	/*Youtube Video*/
	function onPlayerReady(event) {
		$('#play-video').on('click', function(e) {
			$(".play-btn").delay(300).fadeOut(200);
			$(".video-preview").delay(300).fadeOut(200);
			player.playVideo();
			e.preventDefault();
		});
	}

	var player;
	function onYouTubePlayerAPIReady() {
		player = new YT.Player('video', {
			events: {
				'onReady': onPlayerReady
			}
		});
	}
</script>

<script>
	$(function(){
		$('.c-slider').slick({
			autoplay: true,
			dots: true,
			infinite: true,
			speed: 300,
			prevArrow:"<span class='slick-prev'></span",
			nextArrow:"<span class='slick-next'></span",
		});
	});
	jQuery('.accordion .open').children('.accordion--content').slideDown();
	jQuery('.accordion--headline').on('click', function(){
		var $this = jQuery(this),
		$li = $this.closest('li'),
		$open = $this.closest('.accordion').find('li.open').not($li);

	  //Close open accordions
	  $open.children('.accordion--content').slideUp();
	  $open.removeClass('open');
	  
	  //Open selected accordion
	  $li.toggleClass('open');
	  $this.next('.accordion--content').slideToggle();
	});
</script>
</body>
</html>