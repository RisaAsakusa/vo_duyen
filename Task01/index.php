<?php $id="index";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<div class="p-top">

	<!-- section1 -->
	<section class="p-top1">
		<div class="l-container">
			<div class="c-title2">
				<h2><img src="assets/image/common/logo_05.png" width="365" height="98" alt=""></h2>
				
			</div>
			<div class="c-text1">
				<p>通学・通勤中、習いごと、出張。いつわたしたちの生活を揺るがすかわからない大地震。家族が一緒にいるときに起こるとは限りません。有事への備えは自主防災の意識から。大きな不安を少しでも和らげるのは、家族の安否と「ここだよ」「無事だよ」の言葉。COCODAYOは、大切な家族をつなぐお守りアプリです。</p>
			</div>		
			<div class="c-video1">
				<div class="video-block">
					<div class="play-btn">
						<a href="" id="play-video"><span>紹介映像を見る</span></a>		
					</div>
					<img src="assets/image/common/img_05.jpg" width="620" height="350" alt="" class="video-preview">
					<iframe id="video" src="//www.youtube.com/embed/QkXFbePs-Iw?enablejsapi=1&html5=1&start=3&rel=0" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</section>

	<!-- section2 -->
	<section class="p-top2">
		<div class="l-container">
			<div class="c-title2">
				<h2><img src="assets/image/common/title_06.png" width="176" height="43" alt=""></h2>
			</div>
			<div class="c-text1">
				<p>『ココダヨ』では、毎月1日 午前10時 に「訓練地震」を実施いたします。 <br>訓練地震では、本当の地震が発生した場合と同様に、ココダヨユーザ全端末に 最大震度予測5弱の地震が発生したという「訓練の」通知を発信いたします。次回実施予定は 2017年6月1日 木曜日 午前10時 です。</p>
			</div>		
			<div class="c-list2">
				<ul>
					<li>
						<time>2017.3.14</time>
						<p>『ココダヨ』アプリiOS版を、1.1.9にバージョンアップしました。</p>
					</li>
					<li>
						<time>2017.3.14</time>
						<p>『ココダヨ』アプリiOS版を、1.1.9にバージョンアップしました。</p>
					</li>
					<li>
						<time>2017.3.14</time>
						<p>『ココダヨ』アプリiOS版を、1.1.9にバージョンアップしました。</p>
					</li>
				</ul>

				<div class="c-link1">
					<a href="/topics.php">トピックス一覧へ</a>
				</div>				
			</div>	
		</div>
	</section>

	<!-- section3 -->
	<section class="p-top3">
		<div class="l-container">
			<div class="c-title2">				
				<h2>
					<img src="assets/image/common/title_05.png" width="731" height="75" alt="" class="pc-only">
					<img src="assets/image/common/title_05-sp.png" width="633" height="241" alt="" class="sp-only">
				</h2>
			</div>
			<div class="c-sliderList">
				<div class="c-sliderList__card">
					<div class="c-slider">
						<div class="c-slider__item">
							<div class="c-Point">
								<div class="c-Point__img">
									<img src="/assets/image/common/img_06.png" width="216" height="412" alt="">
								</div>
								<div class="c-Point__text">
									<div class="c-title3">
										<span>POINT 1</span>
										<h3>震度５弱以上を即通知。</h3>
									</div>
									<div class="c-text1">
										<p>大きな地震警報発令時は、アプリ上辺の背景が赤になります。</p>
									</div>
								</div>
							</div>
						</div>
						<div class="c-slider__item">
							<div class="c-Point">
								<div class="c-Point__img">
									<img src="/assets/image/common/img_07.png" width="216" height="412" alt="">
								</div>
								<div class="c-Point__text">
									<div class="c-title3">
										<span>POINT 1</span>
										<h3>震度５弱以上を即通知。</h3>
									</div>
									<div class="c-text1">
										<p>左に震央震度、右に発生した日時、震央の場所、マグニチュードが表示されます。</p>
									</div>
								</div>
							</div>
						</div>
						<div class="c-slider__item">
							<div class="c-Point">
								<div class="c-Point__img">
									<img src="/assets/image/common/img_08.png" width="234" height="412" alt="">
								</div>
								<div class="c-Point__text">
									<div class="c-title3">
										<span>POINT 1</span>
										<h3>震度５弱以上を即通知。</h3>
									</div>
									<div class="c-text1">
										<p>無事を知らせるように求めるボタンが表示されるので、押せる状態であれば無事を知らせてください。</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="c-sliderList__card">
					<div class="c-slider">
						<div class="c-slider__item">
							<div class="c-Point">
								<div class="c-Point__img">
									<img src="/assets/image/common/img_09.png" width="216" height="413" alt="">
								</div>
								<div class="c-Point__text">
									<div class="c-title3">
										<span>POINT 2</span>
										<h3>地震の時だけ居場所を表示。</h3>
									</div>
									<div class="c-text1">
										<p>通常時、位置は非表示になっています。「都道府県」「市区町村」「町区、番地」レベルに変更可能です。</p>
									</div>
								</div>
							</div>
						</div>
						<div class="c-slider__item">
							<div class="c-Point">
								<div class="c-Point__img">
									<img src="/assets/image/common/img_10.png" width="230" height="413" alt="">
								</div>
								<div class="c-Point__text">
									<div class="c-title3">
										<span>POINT 2</span>
										<h3>地震の時だけ居場所を表示。</h3>
									</div>
									<div class="c-text1">
										<p>住所が詳しい表示になり、家族の場所が特定しやすくなります。住所をタップすると、地図アプリが開き、位置が表示されます。</p>
									</div>
								</div>
							</div>
						</div>
						<div class="c-slider__item">
							<div class="c-Point">
								<div class="c-Point__img">
									<img src="/assets/image/common/img_11.png" width="216" height="413" alt="">
								</div>
								<div class="c-Point__text">
									<div class="c-title3">
										<span>POINT 2</span>
										<h3>地震の時だけ居場所を表示。</h3>
									</div>
									<div class="c-text1">
										<p>住所をタップすると、地図アプリが開き、位置が表示されます。</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="c-sliderList__card">
					<div class="c-slider c-slider--noline">
						<div class="c-slider__item">
							<div class="c-Point">
								<div class="c-Point__img">
									<img src="/assets/image/common/img_12.png" width="216" height="413" alt="">
								</div>
								<div class="c-Point__text">
									<div class="c-title3">
										<span>POINT 3</span>
										<h3>家族とチャットができます。</h3>
									</div>
									<div class="c-text1">
										<p>大きな地震警報発令時は、アプリ上辺の背景が赤になります。</p>
									</div>
								</div>
							</div>
						</div>
						<div class="c-slider__item">
							<div class="c-Point">
								<div class="c-Point__img">
									<img src="/assets/image/common/img_13.png" width="216" height="413" alt="">
								</div>
								<div class="c-Point__text">
									<div class="c-title3">
										<span>POINT 3</span>
										<h3>家族とチャットができます。</h3>
									</div>
									<div class="c-text1">
										<p>自分の家族とチャットします。電話連絡よりも短時間で全員で状況を共有します。</p>
									</div>
								</div>
							</div>
						</div>
						<div class="c-slider__item">
							<div class="c-Point">
								<div class="c-Point__img">
									<img src="/assets/image/common/img_14.png" width="216" height="413" alt="">
								</div>
								<div class="c-Point__text">
									<div class="c-title3">
										<span>POINT 3</span>
										<h3>家族とチャットができます。</h3>
									</div>
									<div class="c-text1">
										<p>家族だけではなく、実家など他のグループの無事も同時に確認でき、チャットでもやり取りできます。</p>
									</div>
								</div>
							</div>
						</div>
						<div class="c-slider__item">
							<div class="c-Point">
								<div class="c-Point__img">
									<img src="/assets/image/common/img_15.png" width="216" height="413" alt="">
								</div>
								<div class="c-Point__text">
									<div class="c-title3">
										<span>POINT 3</span>
										<h3>家族とチャットができます。</h3>
									</div>
									<div class="c-text1">
										<p>緊急時こそ安全を確認しあう、家族など密接なグループ向け専用なので、日常でも安心してチャットできます。</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</div>
	</section>

	<!-- section4 -->
	<section class="p-top4">
		<div class="l-container">
			<div class="c-title2">
				<h2><img src="assets/image/common/title_04.png" width="371" height="43" alt=""></h2>
			</div>
			<div class="c-col2">
				<img class="img1" src="assets/image/common/img_03.png" width="376" height="140" alt="">
				<img src="assets/image/common/img_04.png" width="375" height="140" alt="">
			</div>
		</div>
	</section>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>